﻿var app = angular.module("myApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider.when("/",
        {
            redirectTo: "/dashboard",
            controller: "dashboardCtrl"
        }).when("/a",
        {
            templateUrl: "/Dashboard/About",
            controller: "dashboardCtrl1"
        }).when("/b",
        {
            templateUrl: "/Dashboard/Contact",
            controller: "dashboardCtrl2"
        });
    $locationProvider.html5Mode(true);
});

app.controller("dashboardCtrl", function ($scope) {
    $scope.msg = "Dashboard Page Message";
});

app.controller("dashboardCtrl1", function ($scope) {
    $scope.msg = "About Page Message";
});

app.controller("dashboardCtrl2", function ($scope) {
    $scope.msg = "Contact Page Message";
});