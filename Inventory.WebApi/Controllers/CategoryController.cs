﻿using Inventory.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Inventory.Service.Service;

namespace Inventory.WebApi.Controllers
{
    public class CategoryController : ApiController
    {
        private CategoryService categoryService = new CategoryService();

        [HttpGet]
        [Route("in/getCategorys")]
        public List<Category> GetCategorys()
        {
            return categoryService.GetCategorys();
        }

        [HttpGet]
        [Route("in/getCategory/{id}")]
        public IHttpActionResult GetCategory(int id)
        {
            var category = categoryService.GetCategory(id);

            if (category == null)
                return NotFound();
            return Ok(category);
        }

        [HttpPost]
        [Route("in/saveCategory")]
        public IHttpActionResult SaveCategory(Category category)
        {
            var isTrue = categoryService.SaveCategory(category);
            if (isTrue == true)
                return Ok();
            return BadRequest();
        }

        [HttpPost]
        [Route("in/updateCategory")]
        public IHttpActionResult UpdateCategory(int id, Category category)
        {
            var isTrue = categoryService.UpdateCategory(id, category);
            if (isTrue == true)
                return Ok();
            return BadRequest();
        }

        [HttpGet]
        [Route("in/deleteCategory/{id}")]
        public IHttpActionResult DeleteCategory(int id)
        {
            var isTrue = categoryService.DeleteCategory(id);
            if (isTrue == true)
                return Ok();
            return BadRequest();
        }
    }
}
