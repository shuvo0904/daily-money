﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Inventory.Data;
using Inventory.Service.Service;

namespace Inventory.WebApi.Controllers
{
    public class PaymentController : ApiController
    {
        private IPaymentService PaymentService = new PaymentService();

        [Authorize]
        [HttpGet]
        [Route("in/getPayments")]
        public List<PaymentCategoryList> GetAllPayment()
        {
            return PaymentService.GetPayments();
        }

        [Authorize]
        [HttpGet]
        [Route("in/getPayment/{id}")]
        public IHttpActionResult GetPayment(int id)
        {
            var payment = PaymentService.GetPayment(id);
            if (payment == null)
                return NotFound();
            return Ok(payment);

        }

        [Authorize]
        [HttpPost]
        [Route("in/savePayment")]
        public IHttpActionResult SaveProduct(Payment payment)
        {
            var isSave = PaymentService.SavePayment(payment);
            if (isSave == true)
                return Ok();
            return BadRequest();
        }

        [Authorize]
        [HttpPost]
        [Route("in/updatePayment")]
        public IHttpActionResult UpdateProduct(int id, Payment payment)
        {
            var isUpdate = PaymentService.UpdatePayment(id, payment);
            if (isUpdate == true)
                return Ok();
            return BadRequest();
        }

        [Authorize]
        [HttpGet]
        [Route("in/deletePayment/{id}")]
        public IHttpActionResult DeletePayment(int id)
        {
            var isDelete = PaymentService.DeletePayment(id);
            if (isDelete == true)
                return Ok();
            return BadRequest();
        }
    }
}

