﻿using Microsoft.Build.Tasks.Deployment.Bootstrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Product = Inventory.Data.Payment;

namespace Inventory.Service.Service
{
    public interface IPaymentService
    {
        List<PaymentCategoryList> GetPayments();
        PaymentCategoryList GetPayment(int id);
        bool SavePayment(Product payment);

        bool UpdatePayment(int id, Product payment);

        bool DeletePayment(int id);
    }
}
