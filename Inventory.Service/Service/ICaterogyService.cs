﻿using Inventory.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Service.Service
{
    interface ICaterogyService
    {
        List<Category> GetCategorys();
        Category GetCategory(int id);

        bool SaveCategory(Category category);

        bool UpdateCategory(int id, Category category);
        bool DeleteCategory(int id);


    }
}
