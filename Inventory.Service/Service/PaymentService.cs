﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Inventory.Data;

namespace Inventory.Service.Service
{
    public class PaymentService :IPaymentService
    {

        private InventoryEntities db = new InventoryEntities();
        public List<PaymentCategoryList> GetPayments()
        {
           var lists =  db.Payments.ToList();
           List<PaymentCategoryList> paymentCategoryList = new List<PaymentCategoryList>();
           foreach (var list in lists)
           {
               var category = db.Categories.Find(list.category_id);

               var item = new PaymentCategoryList()
               {
                   PaymentId = list.payment_id,
                   Amount = list.amount,
                   Details = list.details,
                   Category = category,
                   CreatedDate = list.createdDate,
                   UpdatedDate = list.updatedDate

               };
               paymentCategoryList.Add(item);
           }

           return paymentCategoryList;
        }

        public PaymentCategoryList GetPayment(int id)
        {

            var payment = db.Payments.FirstOrDefault(x => x.payment_id == id);

            var category = db.Categories.Find(payment.category_id);

            PaymentCategoryList paymentCategoryList = new PaymentCategoryList()
            {
                PaymentId = payment.payment_id,
                Amount = payment.amount,
                Details = payment.details,
                Category = category,
                CreatedDate = payment.createdDate,
                UpdatedDate = payment.updatedDate
            };
            return paymentCategoryList;

        }

        public bool SavePayment(Data.Payment payment)
        {
            try
            {
                db.Payments.Add(payment);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdatePayment(int id, Data.Payment payment)
        {
            try
            {
                db.Entry(payment).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeletePayment(int id)
        {
            try
            {
                var payment = db.Payments.Find(id);
                if (payment == null)
                    return false;

                db.Entry(payment).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

public class PaymentCategoryList
{
    public int PaymentId { get; set; }
    public string Amount { get; set; }

    public Category Category { get; set; }
  
    public string Details { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? UpdatedDate { get; set; }
}